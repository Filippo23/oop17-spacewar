package game;

/**
 * Current state of the game.
 */
public enum GameState {
    /**
     * Game running.
     */
    RUNNING,

    /**
     * Game ended.
     */
    ENDED,

    /**
     * Game lost.
     */
    LOST;
}
